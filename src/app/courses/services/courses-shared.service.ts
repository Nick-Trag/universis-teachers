import { Injectable, EventEmitter} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {CoursesGradesModalComponent} from '../components/courses-grades-modal/courses-grades-modal.component';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class CoursesSharedService {
  private modalRef: BsModalRef;
  public choice: string;
  config = {
    ignoreBackdropClick: true,
    keyboard: false,
    initialState: null,
    class: 'modal-content-base'
  };

  constructor(private modalService: BsModalService) {}

  importCourseExamGradesDialog(courseExamId) {
    const initialState  = {
      courseExamId: courseExamId
    };
    this.modalRef =  this.modalService.show(CoursesGradesModalComponent, Object.assign({}, this.config, {
      class: 'modal-content-grades modal-lg', initialState
    }));
    return new Promise<boolean>((resolve, reject) =>
      this.modalRef.content.result.subscribe((result) => {
        resolve(result);
    } ));

  }

}

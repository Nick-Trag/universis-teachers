import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../../services/messages.service';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';


@Component({
  selector: 'app-send-message-to-student',
  templateUrl: './send-message-to-student.component.html'
})
export class SendMessageToStudentComponent implements OnInit {

  @Input() courseClassId: number;
  @Input() studentId: number;
  @Input() showMessageForm = false;
  public messageModel = {
    body: null,
    subject: null
  };

  @Output() succesfulSend = new EventEmitter<boolean>();

  constructor(private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _messagesService: MessagesService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translate: TranslateService) { }

  ngOnInit() {
  }

  initData() {
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }

  save() {
    this._modalService.showDialog( this._translate.instant('Modals.ModalSendMessageTitle'),
      this._translate.instant('Modals.ModalSendMessageMessage'),
      DIALOG_BUTTONS.OkCancel)
      .then(res => {
        if (res === 'ok') {
          this._loadingService.showLoading();
          this._messagesService.sendMessageToStudent(this.courseClassId, this.studentId, this.messageModel)
            .subscribe(msgSend => {
              // complete the action
              this.initData();

              this._loadingService.hideLoading();
              this._toastService.show( this._translate.instant('Modals.SuccessfulSendMessageTitle'),
                this._translate.instant('Modals.SuccessfulSendMessageMessage'));
              let container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-success');
              }
              this.messageSent(true);
            }, (err) => {
              this._loadingService.hideLoading();
              this._toastService.show( this._translate.instant('Modals.FailedSendMessageTitle'),
                this._translate.instant('Modals.FailedSendMessageMessage'));
              let container = document.body.getElementsByClassName('universis-toast-container')[0];
              if (container != null) {
                container.classList.add('toast-error');
              }
              this.messageSent(false);
            });
        }
      });
  }
  messageSent(succesful: boolean) {
    this.succesfulSend.emit(succesful);
  }

}
